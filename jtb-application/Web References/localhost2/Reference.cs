﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.42000.
// 
#pragma warning disable 1591

namespace jtb_application.localhost2 {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.Xml.Serialization;
    using System.ComponentModel;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="RoomRetrieveSoap", Namespace="http://tempuri.org/")]
    public partial class RoomRetrieve : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback GetRoomOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public RoomRetrieve() {
            this.Url = global::jtb_application.Properties.Settings.Default.jtb_application_localhost2_RoomRetrieve;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event GetRoomCompletedEventHandler GetRoomCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetRoom", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public Rooms[] GetRoom(string hotelName, string roomT, System.DateTime checkin, System.DateTime checkout) {
            object[] results = this.Invoke("GetRoom", new object[] {
                        hotelName,
                        roomT,
                        checkin,
                        checkout});
            return ((Rooms[])(results[0]));
        }
        
        /// <remarks/>
        public void GetRoomAsync(string hotelName, string roomT, System.DateTime checkin, System.DateTime checkout) {
            this.GetRoomAsync(hotelName, roomT, checkin, checkout, null);
        }
        
        /// <remarks/>
        public void GetRoomAsync(string hotelName, string roomT, System.DateTime checkin, System.DateTime checkout, object userState) {
            if ((this.GetRoomOperationCompleted == null)) {
                this.GetRoomOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetRoomOperationCompleted);
            }
            this.InvokeAsync("GetRoom", new object[] {
                        hotelName,
                        roomT,
                        checkin,
                        checkout}, this.GetRoomOperationCompleted, userState);
        }
        
        private void OnGetRoomOperationCompleted(object arg) {
            if ((this.GetRoomCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetRoomCompleted(this, new GetRoomCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.8.3752.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class Rooms {
        
        private int roomIDField;
        
        private int hotelIDField;
        
        private string roomTypeField;
        
        /// <remarks/>
        public int RoomID {
            get {
                return this.roomIDField;
            }
            set {
                this.roomIDField = value;
            }
        }
        
        /// <remarks/>
        public int HotelID {
            get {
                return this.hotelIDField;
            }
            set {
                this.hotelIDField = value;
            }
        }
        
        /// <remarks/>
        public string RoomType {
            get {
                return this.roomTypeField;
            }
            set {
                this.roomTypeField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    public delegate void GetRoomCompletedEventHandler(object sender, GetRoomCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.8.3752.0")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetRoomCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetRoomCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public Rooms[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((Rooms[])(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591