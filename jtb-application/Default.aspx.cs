﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Web.Services;
using System.Data;

using Microsoft.AspNet.Identity;


namespace jtb_application
{
    public partial class _Default : Page
    {

        private SqlConnection conn = new SqlConnection("Data Source=(LocalDb)\\MSSQLLocalDB;AttachDbFilename=C:\\Users\\db044432\\source\\repos\\JTB-PROJECT\\jtb-application\\App_Data\\aspnet-jtb-application-20191211093657.mdf;Initial Catalog=aspnet-jtb-application-20191211093657;Integrated Security=True");
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind_ddlCountry();
                Bind_ddlState1();
                Bind_ddlState2();
            }

        }

        protected void bookBtn_Click(object sender, EventArgs e)
        {

        }
        public void Bind_ddlCountry()
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("SELECT Country, Id FROM Country", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            ddlcountry.DataSource = dr;
            ddlcountry.Items.Clear();
            ddlcountry.Items.Add("--Please select country--");
            ddlcountry.DataTextField = "Country";
            ddlcountry.DataValueField = "Id";
            ddlcountry.DataBind();
            conn.Close();
        }

        public void Bind_ddlState()
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("select state, stateID from CountryState where CountryId= '" + ddlcountry.SelectedValue + "'", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            ddlstate.DataSource = dr;
            ddlstate.Items.Clear();
            ddlstate.Items.Add("--Please select state--");
            ddlstate.DataTextField = "state";
            ddlstate.DataValueField = "state";
            ddlstate.DataBind();
            conn.Close();
        }

        public void Bind_ddlState2()
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("select state, stateID from CountryState", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            ddlstate2.DataSource = dr;
            ddlstate2.Items.Clear();
            ddlstate2.Items.Add("--Please select state--");
            ddlstate2.DataTextField = "state";
            ddlstate2.DataValueField = "state";
            ddlstate2.DataBind();
            conn.Close();
        }

        public void Bind_ddlState1()
        {
            conn.Open();
            SqlCommand cmd = new SqlCommand("select state, stateID from CountryState", conn);
            SqlDataReader dr = cmd.ExecuteReader();
            ddlstate1.DataSource = dr;
            ddlstate1.Items.Clear();
            ddlstate1.Items.Add("--Please select state--");
            ddlstate1.DataTextField = "state";
            ddlstate1.DataValueField = "state";
            ddlstate1.DataBind();
            conn.Close();
        }

        protected void ddlcountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            Bind_ddlState();
        }

        protected void hotel_btn_Click(object sender, EventArgs e)
        {
            //Global.checkin=Convert.ToDateTime(check_in.Text);
            //Global.checkout = Convert.ToDateTime(check_out.Text);
            Application["DestCountry"] = ddlcountry.SelectedValue.ToString();
            Application["DestState"] = ddlstate.SelectedValue.ToString();
            Application["CheckIn"] = check_in.Text.ToString();
            Application["CheckOut"] = check_out.Text.ToString();
            Application["Members"] = ddlMembers.SelectedValue.ToString();
            //Application["Duration"] = ddlDuration.SelectedValue.ToString();
            Response.Redirect("Hotels.aspx", false);
        }

        protected void flightbtn_Click(object sender, EventArgs e)
        {
            
                //Global.deptDateG = Convert.ToDateTime(departureflight.Text.ToString());
                //Global.returnDateG = Convert.ToDateTime(returnflight.Text.ToString());
                Session["DepartureCity"] = ddlstate2.SelectedValue.ToString();
                Session["ArrivalCity"] = ddlstate1.SelectedValue.ToString();
                Session["DeptDate"] = departureflight.Text.ToString();

                Session["FlightType"] = typeddl.SelectedValue.ToString();
                if (typeddl.SelectedValue.ToString().Equals("Round Trip"))
                {
                    Session["RetDate"] = returnflight.Text.ToString();
                }
                Session["AdultPassenger"] = ddlAdults.SelectedValue.ToString();
                Session["ChildPassenger"] = ddlChild.SelectedValue.ToString();
                Session["Class"] = ddlClass.SelectedValue.ToString();
                Response.Redirect("Flights.aspx", false);
            
            
        }
        protected void Unnamed_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            Context.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

    }    
}