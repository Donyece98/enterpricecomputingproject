﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightAPI.Models;

namespace jtb_application
{
    public partial class FlightConfirmation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["SeatCost"] = 0.0;
            int adultpass = Convert.ToInt32((String)this.Session["AdultPassenger"]);
            int childpass = Convert.ToInt32((String)this.Session["ChildPassenger"]);

            int totalPass = adultpass + childpass;
            Label passCount = new Label();
            passCount.Text = Convert.ToString(totalPass);


            List<FlightBookingReply.Traveller> travelerList = (List<FlightBookingReply.Traveller>)this.Session["TravelerList"];
            travellergv.DataSource = travelerList;
            travellergv.AutoGenerateColumns = false;           
            travellergv.DataBind();

            bookingType.Text = (String)this.Session["FlightType"];
            depflightNo.Text = (String)this.Session["DepFlightNo"];
            deptcity.Text = (String)this.Session["DepartureCity"];
            deptDate.Text = (String)this.Session["DeptTime"];
            arrcity.Text = (String)this.Session["ArrivalCity"];
            arrdate.Text = (String)this.Session["DeptArrTime"];
            //adding to db

            for (int i = 1; i < (travelerList.Count + 1); i++)
            {

                FlightBookingReply.FlightBookingInfo bookingInput = new FlightBookingReply.FlightBookingInfo();


                bookingInput.DepFlightNo = depflightNo.Text;
                bookingInput.DeptTime = DateTime.Parse(deptDate.Text.ToString());

                SeatRetrieve.SeatRetrieve getseat2 = new SeatRetrieve.SeatRetrieve();
                SeatRetrieve.SeatInfo seatsavailable2 = getseat2.GetSeats((String)this.Session["DepFlightNo"], DateTime.Parse((String)this.Session["DeptTime"]));
                seatNo2.Text = "Seat Number: " + seatsavailable2.SeatID;

                Session["SeatCost"] = seatsavailable2.SeatPrice;
                totalCost.Text = "Total Cost: $" + this.Session["SeatCost"].ToString();

                bookingInput.SeatNo = seatsavailable2.SeatID.ToString();
                bookingInput.ArriTime = DateTime.Parse(arrdate.Text.ToString());
                bookingInput.FlightCost = (float)this.Session["SeatCost"];
                FlightBookingReply.Traveller travelinput = travelerList[i - 1];

                FlightBookingReply.FlightBookingReply replyobj = new FlightBookingReply.FlightBookingReply();
                replyobj.AddFlight(bookingInput, travelinput);
            }

            if (Session["FlightType"].Equals("Round Trip"))
            {


                //create round trip info
                retflightno.Text = (String)this.Session["RetFlightNo"];
                retdepcity.Text = (String)this.Session["ArrivalCity"];
                retdeptdate.Text = (String)this.Session["RetTime"];
                retarrcity.Text = (String)this.Session["DepartureCity"];
                retarrdate.Text = (String)this.Session["RetArrTime"];

                for (int i = 1; i < (travelerList.Count + 1); i++)
                {

                    FlightBookingReply.FlightBookingInfo bookingInputRet = new FlightBookingReply.FlightBookingInfo();


                    bookingInputRet.DepFlightNo = retflightno.Text;
                    bookingInputRet.DeptTime = DateTime.Parse(retdeptdate.Text.ToString());

                    SeatRetrieve.SeatRetrieve getseat3 = new SeatRetrieve.SeatRetrieve();
                    SeatRetrieve.SeatInfo seatsavailable3 = getseat3.GetSeats((String)this.Session["RetFlightNo"], DateTime.Parse((String)this.Session["RetTime"]));
                    seatNo3.Text = "Seat Number: " + seatsavailable3.SeatID;

                    Session["SeatCost"] = seatsavailable3.SeatPrice;
                    returntotalcost.Text = "Total Cost: $" + this.Session["SeatCost"].ToString();

                    bookingInputRet.SeatNo = seatsavailable3.SeatID.ToString();
                    bookingInputRet.ArriTime = DateTime.Parse(retarrdate.Text.ToString());
                    bookingInputRet.FlightCost = (float)this.Session["SeatCost"];
                    FlightBookingReply.Traveller travelinput2 = travelerList[i - 1];

                    FlightBookingReply.FlightBookingReply replyobj = new FlightBookingReply.FlightBookingReply();
                    replyobj.AddFlight(bookingInputRet, travelinput2);
                }
            }
        }

        protected void travellergv_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void travellergv_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {

        }

        protected void redirectbtn_Click(object sender, EventArgs e)
        {

            Response.Redirect("home.aspx");
        }

        protected void checkoutbtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx/#ho1");
        }
    }
}