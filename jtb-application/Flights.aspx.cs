﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FlightAPI.Models; 

namespace jtb_application
{
    public partial class Flights : System.Web.UI.Page
    {
        FlightInfo flight = new FlightInfo();

        protected void Page_Load(object sender, EventArgs e)
        {
            FlightRetrieveResults.FlightRetrieveResults webservobj = new FlightRetrieveResults.FlightRetrieveResults();
            gvFlight.DataSource = webservobj.GetFlight((String)this.Session["ArrivalCity"], (String)this.Session["DepartureCity"], Convert.ToDateTime((String)this.Session["DeptDate"]).Date);
            gvFlight.DataBind();
        }


        protected void gvFlight_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = gvFlight.SelectedRow;
            Session["DepFlightNo"] = row.Cells[1].Text.ToString();
            Session["DeptTime"] = row.Cells[7].Text.ToString();
            Session["DeptArrTime"] = row.Cells[8].Text.ToString();

        }

        protected void gvFlight_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            GridViewRow row = gvFlight.Rows[e.NewSelectedIndex];
        }

        protected void bookbtn_Click(object sender, EventArgs e)
        {
            if (Session["FlightType"].ToString().Equals("One Way"))
            {

                if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated == false)
                {
                    Response.Redirect("Login.aspx", false);
                }
                else
                {
                    Response.Redirect("FlightBooking.aspx", false);
                }

            }
            else if (Session["FlightType"].ToString().Equals("Round Trip"))
            {
                Response.Redirect("ReturnFlight.aspx");
            }
        }
    }
}