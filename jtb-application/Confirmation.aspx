﻿<%@ Page Language="C#"  MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Confirmation.aspx.cs" Inherits="jtb_application.Confirmation" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent"> 
   <link rel="stylesheet" href="../assets/css/style.css" />
    <div id="main-container" style="margin-top:120px;">
        <div id="confirmation_info">
        <div class="gallary-header text-center">
        <div class="container ">
        </div>

        <h2>Hotel Booking Confirmation</h2>
        <br />
        <br />
        <div class="row ">
         
            <div class="col-sm-4" >
                <div class="gallary-header text-center">


                    <div class="service-content text-left">
                        <h4>Name</h4>
                        <asp:Label style="display: inline;" runat="server" ID="name_info"></asp:Label>
                        <%--<p style="display: inline;">Leathon Gregory</p>--%>
                        <br />
                        <hr />
                        <h4>Email</h4>
                        <asp:Label style="display: inline;" runat="server" ID="email_info"></asp:Label>
                        <%--<p style="display: inline;">leathongregory@gmail.com</p>--%>
                        <br />
                        <hr />
                        <h4>Hotel</h4>
                        <asp:Label style="display: inline;" runat="server" ID="hotelName"></asp:Label>
                        <%--<p style="display: inline;">Ac Mariott</p>--%>
                        <hr />
                        <h4>Location</h4>
                        <asp:Label style="display: inline;" runat="server" ID="hotelLocation"></asp:Label>

                        <%--<p style="display: inline;">Kingston</p>--%>
                        <hr />
                        <h4>Cost:</h4>
                        $<asp:Label  style="display: inline;" runat="server" ID="total_cost"></asp:Label>
                        <%--<p style="display: inline;">$123,800.00</p>--%>
                         <hr />
                    </div>
                </div>

            </div>
            <div class="col-sm-4">
                <div class="gallary-header text-center">
                    <div class="service-content text-left">
                        <h4>Room No.</h4>
                        <asp:Label style="display: inline;" runat="server" ID="room_no"></asp:Label>
                        <%--<p style="display: inline;">2201</p>--%>
                        <br />
                        <hr />
                        <h4>Room Type</h4>
                        <asp:Label style="display: inline;" runat="server" ID="roomType"></asp:Label>
                        <%--<p style="display: inline;">Beach</p>--%>
                        <br />
                        <hr />
                        <h4>Check-in Date</h4>
                        <asp:Label style="display: inline;" runat="server" ID="checkin"></asp:Label>
                        <%--<p style="display: inline;">11/12/19</p>--%>
                        <hr />
                        <h4>Check-out Date</h4>
                        <asp:Label style="display: inline;" runat="server" ID="checkout"></asp:Label>
                        <%--<p style="display: inline;">11/20/19</p>--%>
                        <hr />
                         <asp:Label runat="server" ID="confirmationlbl"></asp:Label>
                        <div class="text-right">
                            <asp:Button runat="server" Text="Checkout" CssClass=" book-btn" id="checkoutbtn" OnClick="checkoutbtn_Click"/>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">
                 <%--<img src="../../assets/images/packages/rooms.png" alt="package-place"/>--%>
            </div>
        </div>
    </div>
    <div class="gtco-section border-bottom" id="contact">
    </div>
        </div>
       
    </div>

    <%--///////////////--%>

</asp:Content>
