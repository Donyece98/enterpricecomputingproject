﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(jtb_application.Startup))]
namespace jtb_application
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
