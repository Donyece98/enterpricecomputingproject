﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace jtb_application
{
    public partial class Booking : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            checkIn.Text = Application["CheckIn"].ToString();
            checkOut.Text = Application["CheckOut"].ToString();
            ddlRoomT.SelectedValue = Application["RoomType"].ToString();
        }

        protected void bookNowbtn_Click(object sender, EventArgs e)
        {
            DateTime par_checkin = DateTime.ParseExact(checkIn.Text.ToString(), "mm/dd/yyyy", null);
            DateTime par_checkout = DateTime.ParseExact(checkOut.Text.ToString(), "mm/dd/yyyy", null);
            TimeSpan ts = par_checkout - par_checkin;
            int days = Convert.ToInt16(ts.Days.ToString());
            totalCost.Text = (days * (float)Convert.ToDecimal((String)this.Session["Cost"])).ToString();


            Session["TotalCost"] = totalCost.Text;
            Session["FirstName"] = f_name.Text;
            Session["LastName"] = l_name.Text;
            Session["RoomType"] = ddlRoomT.SelectedValue.ToString();
            Session["CheckIn"] = checkIn.Text.ToString();
            Session["CheckOut"] = checkOut.Text.ToString();



            Response.Redirect("Confirmation.aspx", false);
        }
    }
}