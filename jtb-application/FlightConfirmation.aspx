﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master"  AutoEventWireup="true" CodeBehind="FlightConfirmation.aspx.cs" Inherits="jtb_application.FlightConfirmation" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent"> 
   <link rel="stylesheet" href="../assets/css/style.css" />
     <div id="main-container" style="margin-top:120px">
         <div id="confirmation_info">
             <div class="gallery-header text-center">
                 <div class="container">

                 </div>

                 <h2>Flight Booking Confirmation</h2>
                 <br />
                 <br />
                 <div class="row">
                     <div class="col-sm-4">
                         <div class="gallary-header text-center">


                             <div class="service-content text-left">
                                 <asp:GridView CssClass="table table-responsive" ID="travellergv" runat="server" BackColor="White" GridLines="None" OnSelectedIndexChanged="travellergv_SelectedIndexChanged" OnSelectedIndexChanging="travellergv_SelectedIndexChanging">
                                     <Columns>
                                         <asp:BoundField HeaderText="First Name" DataField="FirstName"/>
                                         <asp:BoundField HeaderText="Last Name" DataField="LastName" />
                                     </Columns>
                                 </asp:GridView>
                                 <h4>Booking Type</h4>
                                 <asp:Label style="display:inline; " runat="server" ID="bookingType"></asp:Label>
                                 <br />
                                 <br />
                                 <h3>Departure Information</h3>
                                 <br />
                                 <br />
                                 <h4>Flight Number</h4>
                                 <asp:Label style="display:inline; " runat="server" ID="depflightNo"></asp:Label>
                                 <br />
                                 <br />
                                 <h4>Seat Number</h4>
                                 <asp:Label style="display:inline; " runat="server" ID="seatNo2"></asp:Label>
                                 <br />
                                 <br />
                                 <h4>Departure City</h4>
                                 <asp:Label style="display:inline; " runat="server" ID="deptcity"></asp:Label>
                                 <br />
                                 <br />
                                 <h4>Departure Date/Time</h4>
                                 <asp:Label style="display:inline; " runat="server" ID="deptDate"></asp:Label>
                                 <br />
                                 <br />
                                 <h4>Arrival City</h4>
                                 <asp:Label style="display:inline; " runat="server" ID="arrcity"></asp:Label>
                                 <br />
                                 <br />
                                 <h4>Arrival Date/Time</h4>
                                 <asp:Label style="display:inline; " runat="server" ID="arrdate"></asp:Label>
                                 <br />
                                 <br />
                                 <h4>Flight Cost</h4>
                                 <asp:Label style="display:inline; " runat="server" ID="totalCost"></asp:Label>
                                 <br />
                                 <br />
                                 <div id="returnflightinfo" runat="server">
                                     <h3>Return Flight Information</h3>
                                     <br />
                                     <br />
                                     <h4>Flight Number</h4>
                                     <asp:Label style="display:inline; " runat="server" ID="retflightno"></asp:Label>
                                     <br />
                                     <br />
                                     <h4>Seat Number</h4>
                                     <asp:Label style="display:inline; " runat="server" ID="seatNo3"></asp:Label>
                                     <br />
                                     <br />
                                     <h4>Departure City</h4>
                                     <asp:Label style="display:inline; " runat="server" ID="retdepcity"></asp:Label>
                                     <br />
                                     <br />
                                     <h4>Departure Date/Time</h4>
                                     <asp:Label style="display:inline; " runat="server" ID="retdeptdate"></asp:Label>
                                     <br />
                                     <br />
                                     <h4>Arrival City</h4>
                                     <asp:Label style="display:inline; " runat="server" ID="retarrcity"></asp:Label>
                                     <br />
                                     <br />
                                     <h4>Arrival Date/Time</h4>
                                     <asp:Label style="display:inline; " runat="server" ID="retarrdate"></asp:Label>
                                     <br />
                                     <br />
                                     <h4>Flight Cost</h4>
                                     <asp:Label style="display:inline; " runat="server" ID="returntotalcost"></asp:Label>
                                     <br />
                                     <br />
                                     <div class="text-right">
                                        <asp:Button runat="server" Text="Checkout" CssClass=" book-btn" id="checkoutbtn" OnClick="checkoutbtn_Click"/>
                                    </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>

             </div>
         </div>
     </div>
</asp:Content>
