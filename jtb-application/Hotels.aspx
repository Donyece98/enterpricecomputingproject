﻿<%@ Page Title="Hotels Page" Language="C#"  MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Hotels.aspx.cs" Inherits="jtb_application.Hotels" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent"> 
    <div style="text-align:center;text-align:center; width:800px;margin-left:auto; margin-right:auto; margin-top:120px;">
            <asp:GridView runat="server" CssClass="table table-responsive" ID="gvHotel" AutoGenerateColumns="false" AutoGenerateSelectButton="true" SelectedIndex="0" DataKeyNames="ID" OnSelectedIndexChanged="gvHotel_SelectedIndexChanged" OnSelectedIndexChanging="gvHotel_SelectedIndexChanging">
                <Columns>
                    <asp:ImageField DataImageUrlField="hotelImg" HeaderText="" ControlStyle-Width="200px" ControlStyle-Height="200px"></asp:ImageField>
                    <asp:BoundField DataField="Name" HeaderText="Hotel" SortExpression="Hotel" ControlStyle-Width="350px" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center"/>
                    <asp:BoundField DataField="Location" HeaderText="Location" SortExpression="Location" ItemStyle-Width="200px" />
                    <asp:BoundField DataField="HotelPrice" HeaderText="Cost per Night" SortExpression="Cost per Night" ItemStyle-Width="200px" />
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="DarkBlue" ForeColor="White" Font-Bold="true" BorderColor="Blue" BorderWidth="1px"/>

            </asp:GridView>
            <br /><br />
            <h4 style="margin-bottom:5px;">Room Type</h4>
            <asp:DropDownList ID="ddlRoomType" runat="server" CssClass="form-control">
                <asp:ListItem Value="Select Room Type">Select Room Type</asp:ListItem>
                <asp:ListItem Value="Garden">Garden View Room</asp:ListItem>
                <asp:ListItem Value="Beach">Beach View Room</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredInsertRoomType" runat="server" ErrorMessage="Please select a room type" ControlToValidate="ddlRoomType" Text="*" ForeColor="Red" Font-Bold="true" InitialValue="Select Image"></asp:RequiredFieldValidator>
            <br />
            <br />
            <asp:Button runat="server" Text="Book Now" ID="bookbtn" OnClick="bookbtn_Click" CssClass="btn"/>
    </div>
</asp:Content>

