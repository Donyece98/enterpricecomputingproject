﻿<%@ Page Title="Flights Page" MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="Flights.aspx.cs" Inherits="jtb_application.Flights" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent"> 
   <div style="text-align:center;text-align:center; width:800px;margin-left:auto; margin-right:auto; margin-top:120px;">
            <asp:GridView runat="server" CssClass="table table-responsive" BackColor="White" GridLines="None" ID="gvFlight" AutoGenerateColumns="true" AutoGenerateSelectButton="true" SelectedIndex="0" DataKeyNames="flightNo" OnSelectedIndexChanged="gvFlight_SelectedIndexChanged" OnSelectedIndexChanging="gvFlight_SelectedIndexChanging">
                <Columns>
                </Columns>
                <HeaderStyle HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="DarkBlue" ForeColor="White" Font-Bold="true" BorderColor="Blue" BorderWidth="1px"/>

            </asp:GridView>
            <br /><br />
            <br />
            <br />
            <asp:Button runat="server" Text="Book Now" ID="bookbtn" OnClick="bookbtn_Click" CssClass="btn"/>
        </div>
</asp:Content>
