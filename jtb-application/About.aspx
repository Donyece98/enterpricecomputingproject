﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="jtb_application.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="../assets/css/style.css" />
    <section id="service" class="service" style="margin-top:120px;">
        <div class="container">

            <div class="service-counter text-center">

                <div class="col-md-4 col-sm-4">
                    <div class="single-service-box">
                        <div class="service-img">
                            <img src="../assets/images/service/s1.png" alt="service-icon" />
                        </div>
                        <!--/.service-img-->
                        <div class="service-content">
                            <h2>
                                <a href="#">Choose amazing tour packages
                                </a>
                            </h2>
                            <p>Must use our tour Planner for breathtaking tour packages!</p>
                        </div>
                        <!--/.service-content-->
                    </div>
                    <!--/.single-service-box-->
                </div>
                <!--/.col-->

                <div class="col-md-4 col-sm-4">
                    <div class="single-service-box">
                        <div class="service-img">
                            <img src="../assets/images/service/s2.png" alt="service-icon" />
                        </div>
                        <!--/.service-img-->
                        <div class="service-content">
                            <h2>
                                <a href="#">book top class hotel
                                </a>
                            </h2>
                            <p>This amazing site helps you book the best hotels all around the world!</p>
                        </div>
                        <!--/.service-content-->
                    </div>
                    <!--/.single-service-box-->
                </div>
                <!--/.col-->

                <div class="col-md-4 col-sm-4">
                    <div class="single-service-box">
                        <div class="statistics-img">
                            <img src="../assets/images/service/s3.png" alt="service-icon" />
                        </div>
                        <!--/.service-img-->
                        <div class="service-content">

                            <h2>
                                <a href="#">online flight booking
                                </a>
                            </h2>
                            <p>Book your flight instantly using TourNest!</p>
                        </div>
                        <!--/.service-content-->
                    </div>
                    <!--/.single-service-box-->
                </div>
                <!--/.col-->

            </div>
            <!--/.statistics-counter-->
        </div>
        <!--/.container-->

    </section>
    <!--/.service-->
    <div class="gallary-header ">
        <h2>About Us
        </h2>
        <p>
            By investing in the technology that helps take the friction out of travel, JTB.com seamlessly connects millions of travelers with memorable experiences, a range of transport options and incredible places to stay - from homes to hotels and much more. As one of the world’s largest travel marketplaces for both established brands and entrepreneurs of all sizes, Booking.com enables properties all over the world to reach a global audience and grow their businesses.
JTB.com is available in 43 languages and offers more than 28 million total reported accommodation listings, including over 6.2 million listings alone of homes, apartments and other unique places to stay. No matter where you want to go or what you want to do, JTB.com makes it easy and backs it all up with 24/7 customer support
        </p>
        <br />
        <br />
        <h2>Incredible choice
        </h2>
        <p>
            Whether you want to stay in a chic city apartment, a luxury beach resort or a cosy B&B in the countryside, Booking.com provides you with amazing diversity and breadth of choice - all in one place.
        </p>
        <br />
        <br />
        <h2>Low rates
        </h2>
        <p>
            Booking.com guarantees to offer you the best available rates. And with our promise to price match, you can rest assured that you’re always getting a great deal.
        </p>
        <br />
        <br />
        <h2>No reservation fees
        </h2>
        <p>
            We don’t charge you any booking fees or add any administrative charges. And in many cases, your booking can be cancelled free of charge.
        </p>
        <br />
        <br />
        <h2>Secure booking
        </h2>
        <p>
            We facilitate hundreds of thousands of transactions every day through our secure platform, and work to the highest standards to guarantee your privacy. 
        </p>
    </div>
    <section id="hot" class="packages">
        <div class="container">

            <!--/.gallery-header-->
        </div>
        <!--/.container-->

    </section>
</asp:Content>
