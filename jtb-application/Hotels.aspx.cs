﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HotelAPI.Models; 

namespace jtb_application
{
    public partial class Hotels : System.Web.UI.Page
    {
        public Hotel selectedHotel = new Hotel();
        protected void Page_Load(object sender, EventArgs e)
        {
            localhost1.HotelRetrieveID webservobj = new localhost1.HotelRetrieveID();
            gvHotel.DataSource = webservobj.GetHotel(Application["DestState"].ToString());
            gvHotel.DataBind();


        }

        protected void gvHotel_SelectedIndexChanged(object sender, EventArgs e)
        {
            GridViewRow row = gvHotel.SelectedRow;
            Session["Name"] = row.Cells[2].Text.ToString();
            Session["Location"] = row.Cells[3].Text.ToString();
            Session["Cost"] = row.Cells[4].Text.ToString();

            //selectedHotel.Name = row.Cells[2].Text;
            //selectedHotel.Location = row.Cells[3].Text; 
            //selectedHotel.HotelPrice = (float)Convert.ToDecimal(row.Cells[4].Text); 
        }


        protected void bookbtn_Click(object sender, EventArgs e)
        {
            //Session["Name"] = selectedHotel.Name;
            //Session["Location"] = selectedHotel.Location; 
            //Application["Cost"] = selectedHotel.HotelPrice;
            Application["RoomType"] = ddlRoomType.SelectedValue.ToString();
            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated == false)
            {
                Response.Redirect("Login.aspx", false);
            }
            else
            {
                Response.Redirect("Booking.aspx", false);
            }
        }

        protected void gvHotel_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
        {
            GridViewRow row = gvHotel.Rows[e.NewSelectedIndex];
        }
    }    
}