﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Site.Master" CodeBehind="FlightBooking.aspx.cs" Inherits="jtb_application.FlightBooking" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent"> 
  <div style="margin-top:120px">
            <!--Auto generated text boxes-->
            <div id="bookingform" runat="server">         
                

                <asp:Label Text="Traveller #" runat="server" ID="travelercnt"></asp:Label>

                <h5>First Name</h5>
                <asp:TextBox ID="fName" runat="server" CssClass="form-control"></asp:TextBox><br />

                <h5>Last Name</h5>
                <asp:TextBox ID="lname" runat="server" CssClass="form-control"></asp:TextBox><br />

                <h5>Date Of Birth</h5>
                <asp:TextBox ID="dob" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox><br /><br />
            </div>
            <h3>Booking Information</h3>
            <br /><br />
             <h5>Booking Type</h5>
            <asp:Label runat="server" Text="Booking Type: " ID="bookingType"></asp:Label><br />    <br />  
      
             <h5>Flight Number</h5>
            <asp:Label runat="server" Text="Flight Number #" ID="depflightNo" ></asp:Label><br /><br />

                <h4>Departure Information</h4><br /><br />

      <h5>Departure CIty</h5>
            <asp:Label runat="server" Text="DeptCity" ID="deptcity"></asp:Label><br /><br />
      <h5>Flight Date / Time</h5>
            <asp:Label runat="server" Text="Flight Date/Time" ID="deptDate"></asp:Label><br /><br />

            <h4>Arrival Information</h4><br /><br />
      <h5>Arrival City</h5>
            <asp:Label runat="server" Text="Arrival City" ID="arrcity"></asp:Label><br /><br />
      <h5>Arrival Date/Time</h5>
            <asp:Label runat="server" Text="Date/Time: " ID="arrdate"></asp:Label>

      <div><br /><br />
              <!--Return Arrival Flight information-->
            <h3>Return Booking Information</h3><br /><br />
            
          <h5>Flight Number</h5>
              <asp:Label runat="server" Text="Flight Number #" ID="retflightno" ></asp:Label><br /><br />

          <h4>Departure Information</h4><br /><br />
            

          <h5>Departure City</h5>
            <asp:Label runat="server" Text="DeptCity" ID="retdepcity"></asp:Label><br /><br />
          <h5>Departure Date/Time</h5>
            <asp:Label runat="server" Text="Flight Date/Time" ID="retdeptdate"></asp:Label><br /><br />

          <h4>Arrival Information</h4><br /><br />
          <h5>Arrival City</h5>
            <asp:Label runat="server" Text="Arrival City" ID="retarrcity"></asp:Label><br /><br />
          <h5>Arrival Date/ Time</h5>
            <asp:Label runat="server" Text="Date/Time: " ID="retarrdate"></asp:Label>
      </div><br />
            <asp:Button runat="server" ID="confirmbtn" Text="Confirm" OnClick="confirmbtn_Click" CssClass="btn" />
        </div>
</asp:Content>


