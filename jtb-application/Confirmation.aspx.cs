﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HotelAPI.Models;

namespace jtb_application
{
    public partial class Confirmation : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            localhost3.HotelBooking booking = new localhost3.HotelBooking();

            name_info.Text = Session["FirstName"].ToString();
            email_info.Text = System.Web.HttpContext.Current.User.Identity.Name.ToString();
            hotelName.Text = (String)this.Session["Name"];
            hotelLocation.Text = (String)this.Session["Location"];
            roomType.Text = (String)this.Session["RoomType"];
            checkin.Text = (String)this.Session["CheckIn"];
            checkout.Text = (String)this.Session["CheckOut"];


            localhost2.RoomRetrieve roomwebserobj = new localhost2.RoomRetrieve();
            localhost2.Rooms[] roomsAvailable = roomwebserobj.GetRoom((String)this.Session["Name"], (String)this.Session["RoomType"], Convert.ToDateTime((String)this.Session["CheckIn"]), Convert.ToDateTime((String)this.Session["CheckOut"]));
            room_no.Text = Convert.ToString(roomsAvailable[0].RoomID);
            total_cost.Text = (String)this.Session["TotalCost"];

            localhost.HotelRetrieve getHotels = new localhost.HotelRetrieve();
            localhost.Hotel[] hotelList = getHotels.GetAllHotels();
            foreach (var item in hotelList)
            {
                if (item.Name.Equals((String)this.Session["Name"]))
                {
                    booking.hotelId = item.ID;
                }
            }

            booking.roomId = roomsAvailable[0].RoomID.ToString();
            booking.userId = email_info.Text;
            booking.bookingCost = float.Parse(total_cost.Text.ToString());
            booking.checkin = DateTime.Parse(checkin.Text.ToString());
            booking.checkout = DateTime.Parse(checkout.Text.ToString());
            booking.available = false;

            localhost3.BookingReply bookreplyobj = new localhost3.BookingReply();
            confirmationlbl.Text = bookreplyobj.AddBooking(booking);
        }

        protected void checkoutbtn_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx/#fl241"); 
        }
    }
}