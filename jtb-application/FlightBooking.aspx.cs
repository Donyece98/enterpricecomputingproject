﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

using FlightAPI.Models;

namespace jtb_application
{
    public partial class FlightBooking : System.Web.UI.Page
    {
        public int tempcounter = new int();
        public List<FlightBookingReply.Traveller> travelerlist = new List<FlightBookingReply.Traveller>();

        protected void Page_Load(object sender, EventArgs e)
        {
            int adultPass = Convert.ToInt32((String)this.Session["AdultPassenger"]);
            int childpass = Convert.ToInt32((String)this.Session["ChildPassenger"]);

            int totalPass = childpass + adultPass;

            Session["counter"] = "1";
            Session["totalPass"] = totalPass.ToString();

            //travelercnt2.Text = "Passenger Amount: " + (string)this.Session["totalPass"]; //+totalPass.ToString(); 
            travelercnt.Text = "Passenger #" + Session["counter"].ToString();
            bookingType.Text = (String)this.Session["FlightType"];
            depflightNo.Text = (String)this.Session["DepFlightNo"];
            deptcity.Text = (String)this.Session["DepartureCity"];
            deptDate.Text = (String)this.Session["DeptTime"];
            arrcity.Text = (String)this.Session["ArrivalCity"];
            arrdate.Text = (String)this.Session["DeptArrTime"];

            if (Session["FlightType"].Equals("Round Trip"))
            {
                retflightno.Text = (String)this.Session["RetFlightNo"];
                retdepcity.Text = (String)this.Session["ArrivalCity"];
                retdeptdate.Text = (String)this.Session["RetTime"];
                retarrcity.Text = (String)this.Session["DepartureCity"];
                retarrdate.Text = (String)this.Session["RetArrTime"];
            }


        }

        protected void confirmbtn_Click(object sender, EventArgs e)
        {
            FlightBookingReply.Traveller traveler = new FlightBookingReply.Traveller();
            tempcounter = int.Parse(Session["counter"].ToString());
            traveler.FirstName = fName.Text;
            traveler.LastName = lname.Text;
            traveler.DOB = DateTime.Parse(dob.Text.ToString());
            travelerlist.Add(traveler);

            tempcounter++;
            Session["counter"] = tempcounter.ToString();

            Session["TravelerList"] = travelerlist;


            if (Session["totalPass"].ToString().Equals(Session["counter"].ToString()))
            {

                //counterlbl.Text = "Sumn Wrong"; 
                Response.Redirect("FlightConfirmation.aspx", false);
            }


            fName.Text = "";
            lname.Text = "";
            dob.Text = "";


            travelercnt.Text = "Passenger #" + Session["counter"].ToString();

        }
    }
}