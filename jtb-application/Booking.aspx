﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Booking.aspx.cs" Inherits="jtb_application.Booking" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent"> 
  <div id="main-container" style="margin-top:120px;">
        <div id="bookingform">
            <h4>Hotel Booking Information</h4><br /><br />
            First Name: <asp:TextBox ID="f_name" runat="server" Cssclass="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator ID="FnameVal" runat="server" ErrorMessage="Please enter First Name" Text="*" ForeColor="Red" ControlToValidate="f_name"></asp:RequiredFieldValidator>
            <br />
            Last Name: <asp:TextBox ID="l_name" runat="server" Cssclass="form-control"></asp:TextBox>
            <asp:RequiredFieldValidator ID="lnameVal" runat="server" ErrorMessage="Please enter Last Name" Text="*" ForeColor="Red" ControlToValidate="l_name"></asp:RequiredFieldValidator>
            <br />
            Check In: <asp:TextBox  runat="server" ID="checkIn" Cssclass="form-control"  data-toggle="datepicker" TextMode="SingleLine" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="checkinVal" ErrorMessage="Please enter valid check in Date" Text="*" ForeColor="Red" ControlToValidate="checkIn"></asp:RequiredFieldValidator>
            <br />
            Check Out: <asp:TextBox  runat="server" ID="checkOut" Cssclass="form-control"  data-toggle="datepicker" TextMode="SingleLine"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="checkoutVal" ErrorMessage="Please enter valid check out date" Text="*" ForeColor="Red" ControlToValidate="checkOut"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ControlToCompare="checkIn" Display="Dynamic" EnableClientScript="true" CultureInvariantValues="true" Type="Date" Operator="GreaterThanEqual" ControlToValidate="checkOut" ErrorMessage="Please enter valid checkout date"></asp:CompareValidator>
            <br />
            Room Type 
            <asp:DropDownList runat="server" ID="ddlRoomT" Cssclass="form-control">
                <asp:ListItem Value="Select Room Type">--Select Room Type--</asp:ListItem>
                <asp:ListItem Value="Garden">Garden Room</asp:ListItem>
                <asp:ListItem Value="Beach">Beach Room</asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlRoomT" InitialValue="Select Room Type" ErrorMessage="Please enter room type" Text="*" ForeColor="Red"></asp:RequiredFieldValidator>
            <asp:ValidationSummary runat="server" ID="bookval" />
            <br />
            <asp:Label runat="server" ID="totalCost"></asp:Label>
            <br />
            <asp:Button Text="Book Now" runat="server" ID="bookNowbtn" OnClick="bookNowbtn_Click" CssClass="btn"/>
        
        
        
        </div>
    </div>
</asp:Content>

