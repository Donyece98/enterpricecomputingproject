﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightAPI.Models
{
    public class FlightInfo
    {
        string flightNo;
        string airline;
        string departureCountry;
        string departureCity;
        string arrivalCountry;
        string arrivalCity;
        DateTime departureDate;
        DateTime arrivalDate;
        int seatCount;
        int availSeats;

        public string FlightNo
        {
            get { return flightNo; }
            set { flightNo = value; }
        }

        public string Airline
        {
            get { return airline; }
            set { airline = value; }
        }

        public string DepCountry
        {
            get { return departureCountry; }
            set { departureCountry = value; }
        }
        public string DepCity
        {
            get { return departureCity; }
            set { departureCity = value; }
        }
        public string ArriCountry
        {
            get { return arrivalCountry; }
            set { arrivalCountry = value; }
        }
        public string ArriCity
        {
            get { return arrivalCity; }
            set { arrivalCity = value; }
        }

        public DateTime DeptDate
        {
            get { return departureDate; }
            set { departureDate = value; }
        }
        public DateTime ArriDate
        {
            get { return arrivalDate; }
            set { arrivalDate = value; }
        }

        public int SeatAmt
        {
            get { return seatCount; }
            set { seatCount = value; }
        }
        public int AvailSeats
        {
            get { return availSeats; }
            set { availSeats = value; }
        }

    }
}