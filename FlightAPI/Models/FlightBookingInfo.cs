﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightAPI.Models
{
    public class FlightBookingInfo
    {
        string bookingID;
        String passID;
        string depflightNo;
        string seatNo;
        DateTime departureDate;
        DateTime arrivalDate;
        float totalCost;

        public string BookingID
        {
            get { return bookingID; }
            set { bookingID = value; }
        }
        public string Passangers
        {
            get { return passID; }
            set { passID = value; }
        }

        public string DepFlightNo
        {
            get { return depflightNo; }
            set { depflightNo = value; }
        }

        public string SeatNo
        {
            get { return seatNo; }
            set { seatNo = value; }
        }

        public DateTime DeptTime
        {
            get { return departureDate; }
            set { departureDate = value; }
        }
        public DateTime ArriTime
        {
            get { return arrivalDate; }
            set { arrivalDate = value; }

        }
        public float FlightCost
        {
            get { return totalCost; }
            set { totalCost = value; }
        }
    }
}