﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightAPI.Models
{
    public class SeatInfo
    {
        string seatID;
        string flightID;
        bool available;
        float seatPrice;

        public string SeatID
        {
            get { return seatID; }
            set { seatID = value; }
        }

        public string FlightID
        {
            get { return flightID; }
            set { flightID = value; }
        }

        public bool Available
        {
            get { return available; }
            set { available = value; }
        }
        public float SeatPrice
        {
            get { return seatPrice; }
            set { seatPrice = value; }
        }
    }
}