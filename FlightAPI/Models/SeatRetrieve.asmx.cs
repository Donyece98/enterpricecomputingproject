﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace FlightAPI.Models
{
    /// <summary>
    /// Summary description for SeatRetrieve
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class SeatRetrieve : System.Web.Services.WebService
    {
        public List<SeatInfo> seats = new List<SeatInfo>();

        [WebMethod]
        public SeatInfo GetSeats(string flightNo, DateTime deptDate)
        {
            FlightAPIEntities flightapi = new FlightAPIEntities();
            Flight flight = new Flight();


            foreach (var item1 in flightapi.Seats)
            {
                if (item1.FlightID.Equals(flightNo))
                {
                    SeatInfo seat = new SeatInfo();
                    seat.FlightID = item1.FlightID;
                    seat.SeatID = item1.SeatId;
                    seat.Available = item1.Available;
                    seat.SeatPrice = (float)item1.SeatCost;

                    seats.Add(seat);
                }
            }
            
            foreach (var item in flightapi.FlightBookings)
            {
                for (int i = 0; i < seats.Count; i++)
                {
                    if ((item.seatNo.Equals(seats[i].SeatID)) && (item.deptDate.Equals(deptDate)))
                    {
                        seats.Remove(seats[i]);
                    }
                }
            }
            
            return seats[0];
        }
    }
}
