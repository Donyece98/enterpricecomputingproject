﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FlightAPI.Models
{
    public class Traveller
    {
        string travelerId;
        string firstName;
        string lastName;
        DateTime dob;


        public string TravelerID
        {
            get { return travelerId; }
            set { travelerId = value; }
        }
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        public DateTime DOB
        {
            get { return dob; }
            set { dob = value; }
        }
    }
}