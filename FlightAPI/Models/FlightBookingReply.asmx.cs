﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace FlightAPI.Models
{
    /// <summary>
    /// Summary description for FlightBookingReply
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FlightBookingReply : System.Web.Services.WebService
    {

        private SqlConnection con;
        private SqlCommand com;

        private void connection()
        {
            con = new SqlConnection("Data Source=JM-DB044432\\SQLEXPRESS;Initial Catalog=FlightAPI;Integrated Security=True;Pooling=False;MultipleActiveResultSets=True;Application Name=EntityFramework");
        }

        [WebMethod]
        public string AddFlight(FlightBookingInfo booking, Traveller traveller)
        {

            connection();
            SqlCommand com1 = new SqlCommand("SELECT BookingId FROM FlightBooking", con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(com1);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            string bookingIDval = ((Convert.ToInt32(dt.Rows[dt.Rows.Count - 1]["BookingId"])) + 1).ToString();

            booking.BookingID = bookingIDval.ToString();
            con.Close();

            SqlCommand com3 = new SqlCommand("SELECT * FROM Traveler", con);
            con.Open();
            SqlDataAdapter sda2 = new SqlDataAdapter(com3);
            DataTable dt2 = new DataTable();
            sda2.Fill(dt2);

            string travelleridval;


            DataRow[] filteredrows = dt2.Select("FirstName LIKE '%" + traveller.FirstName + "%' AND LastName LIKE '%" + traveller.LastName + "%'");
            if (filteredrows is null)
            {
                travelleridval = ((Convert.ToInt32(dt2.Rows[dt2.Rows.Count - 1]["TravelerId"])) + 1).ToString();
                SqlCommand com2 = new SqlCommand("insert into Traveler (Travelerid, FirstName, LastName, DOB) values (@travelerid, @firstname, @lastname, @dob)", con);
                com2.Parameters.AddWithValue("@travelerid", traveller.TravelerID);
                com2.Parameters.AddWithValue("@firstname", traveller.FirstName);
                com2.Parameters.AddWithValue("@lastname", traveller.LastName);
                com2.Parameters.AddWithValue("@dob", traveller.DOB);

                int x = com2.ExecuteNonQuery();

            }
            else
            {
                travelleridval = filteredrows[filteredrows.Count() - 1]["TravelerId"].ToString();
            }
            con.Close();

            traveller.TravelerID = travelleridval.ToString();


            com = new SqlCommand("INSERT INTO FlightBooking (BookingId, travelerId, depFlightNo, seatNo, deptDate, arriDate, totalCost) values (@bookingid, @travelerid, @depflightno, @seatno, @deptdate, @arridate, @totalcost)", con);
            com.Parameters.AddWithValue("@bookingid", booking.BookingID);
            com.Parameters.AddWithValue("@travelerid", traveller.TravelerID);
            com.Parameters.AddWithValue("@depflightno", booking.DepFlightNo);
            com.Parameters.AddWithValue("@seatno", booking.SeatNo);
            com.Parameters.AddWithValue("@deptdate", booking.DeptTime);
            com.Parameters.AddWithValue("@arridate", booking.ArriTime);
            com.Parameters.AddWithValue("@totalcost", booking.FlightCost);

            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {
                return "New Booking Added Successfully";
            }
            else
            {
                return "Booking Not Added";
            }


        }
    }
}
