﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace FlightAPI.Models
{
    /// <summary>
    /// Summary description for FlightRetrieveAll
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FlightRetrieveAll : System.Web.Services.WebService
    {

        List<FlightInfo> flights = new List<FlightInfo>();

        [WebMethod]
        public List<FlightInfo> GetAllFlights()
        {

            FlightAPIEntities flightapi = new FlightAPIEntities();
            foreach (var item in flightapi.Flights)
            {
                FlightInfo flight = new FlightInfo
                {
                    FlightNo = item.FlightNo,
                    Airline = item.Airline,
                    DepCountry = item.DeptCountry,
                    DepCity = item.DeptCity,
                    ArriCountry = item.ArriCountry,
                    ArriCity = item.ArriCity,
                    DeptDate = item.DeptDate,
                    ArriDate = item.ArriDate,
                    SeatAmt = item.totalSeat,
                    AvailSeats = item.availSeat
                };

                flights.Add(flight);
            }
            return flights;
        }
    }
}
