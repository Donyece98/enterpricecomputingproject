﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FlightAPI.Models; 

namespace FlightAPI.Controllers
{
    public class FlightReplyController : ApiController
    {
        static FlightBookingReply flightreply = new FlightBookingReply();

        [HttpPost]
        public string AddBooking(FlightBookingInfo booking, Traveller traveller)
        {
            var response = flightreply.AddFlight(booking, traveller);
            return response;
        }
    }
}
