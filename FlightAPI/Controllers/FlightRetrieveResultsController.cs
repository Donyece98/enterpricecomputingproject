﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FlightAPI.Models; 

namespace FlightAPI.Controllers
{
    public class FlightRetrieveResultsController : ApiController
    {
        public List<FlightInfo> flights = new List<FlightInfo>();

        [HttpGet]
        public List<FlightInfo> GetFlight(string arrivalCity, string destCity, DateTime deptDate)
        {
            FlightAPIEntities flightapi = new FlightAPIEntities();
            foreach (var item in flightapi.Flights)
            {
                if ((item.ArriCity.Equals(arrivalCity)) && (item.DeptCity.Equals(destCity)) && (item.DeptDate.Date.Equals(deptDate)))
                {
                    FlightInfo flight = new FlightInfo
                    {
                        FlightNo = item.FlightNo,
                        Airline = item.Airline,
                        DepCountry = item.DeptCountry,
                        DepCity = item.DeptCity,
                        ArriCountry = item.ArriCountry,
                        ArriCity = item.ArriCity,
                        DeptDate = item.DeptDate,
                        ArriDate = item.ArriDate,
                        SeatAmt = item.totalSeat,
                        AvailSeats = item.availSeat
                    };

                    flights.Add(flight);
                }
            }
            return flights;
        }
    }
}
