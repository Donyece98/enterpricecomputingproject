﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FlightAPI.Models; 

namespace FlightAPI.Controllers
{
    public class FlightRetrieveController : ApiController
    {
        public List<FlightInfo> flightList = new List<FlightInfo>();

        [HttpGet]
        public List<FlightInfo> GetAllFlights()
        {
            FlightAPIEntities flightapi = new FlightAPIEntities();
            foreach (var item in flightapi.Flights)
            {
                FlightInfo flight = new FlightInfo
                {
                    FlightNo = item.FlightNo,
                    Airline = item.Airline,
                    DepCountry = item.DeptCountry,
                    DepCity = item.DeptCity,
                    ArriCountry = item.ArriCountry,
                    ArriCity = item.ArriCity,
                    DeptDate = item.DeptDate,
                    ArriDate = item.ArriDate,
                    SeatAmt = item.totalSeat,
                    AvailSeats = item.availSeat
                };

                flightList.Add(flight);
            }
            return flightList;
        }
    }
}
