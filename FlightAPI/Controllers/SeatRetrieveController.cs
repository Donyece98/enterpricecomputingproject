﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FlightAPI.Models; 

namespace FlightAPI.Controllers
{
    public class SeatRetrieveController : ApiController
    {
        public List<SeatInfo> seats = new List<SeatInfo>();

        [HttpGet]
        public SeatInfo GetSeats(string flightNo, DateTime deptDate)
        {
            FlightAPIEntities flightapi = new FlightAPIEntities();
            FlightInfo flight = new FlightInfo();


            foreach (var item1 in flightapi.Seats)
            {
                if (item1.FlightID.Equals(flightNo))
                {
                    SeatInfo seat = new SeatInfo();
                    seat.FlightID = item1.FlightID;
                    seat.SeatID = item1.SeatId;
                    seat.Available = item1.Available;
                    seat.SeatPrice = (float)item1.SeatCost;

                    seats.Add(seat);
                }
            }

            foreach (var item in flightapi.FlightBookings)
            {
                for (int i = 0; i < seats.Count; i++)
                {
                    if ((item.seatNo.Equals(seats[i].SeatID)) && (item.deptDate.Equals(deptDate)))
                    {
                        seats.Remove(seats[i]);
                    }
                }
            }
            return seats[0];
        }
    }
}
