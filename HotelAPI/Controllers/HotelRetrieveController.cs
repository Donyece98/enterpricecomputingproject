﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HotelAPI.Models; 

namespace HotelAPI.Controllers
{
    public class HotelRetrieveController : ApiController
    {
        public List<Hotel> hotelList = new List<Hotel>();

        [HttpGet]
        public List<Hotel> GetAllHotel()
        {
            HotelAPIEntities hotelapi = new HotelAPIEntities();

            foreach (var item in hotelapi.HotelInfoes)
            {

                Hotel hotel = new Hotel();
                hotel.ID = item.hotelID;
                hotel.Name = item.hotelName;
                hotel.Location = item.hotelLocation;
                hotel.HotelPrice = (float)item.hotelCost;
                hotel.GardenRm = (int)item.hotelAvailGarden;
                hotel.BeachRm = (int)item.hotelAvailBeach;
                hotel.AllInclusive = item.hotelAllInclusive;
                hotel.HotelImg = item.hotelImage;

                hotelList.Add(hotel);

            }
            return hotelList;
        }
    }
}
