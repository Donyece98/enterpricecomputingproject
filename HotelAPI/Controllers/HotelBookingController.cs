﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HotelAPI.Models; 

namespace HotelAPI.Controllers
{
    public class HotelBookingController : ApiController
    {
        static BookingReply bookreply = new BookingReply();

        [HttpPost]
        public string addBooking(HotelBooking booking)
        {
            var response = bookreply.AddBooking(booking);
            return response;
        }
    }
}
