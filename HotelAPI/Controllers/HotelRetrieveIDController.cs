﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HotelAPI.Models; 


namespace HotelAPI.Controllers
{
    public class HotelRetrieveIDController : ApiController
    {
        public List<Hotel> hotelList1 = new List<Hotel>();

        [HttpGet]
        public List<Hotel> GetHotel(string location)
        {
            HotelAPIEntities hotelapi1 = new HotelAPIEntities();
            //bool found = new bool(); 
            foreach (var item in hotelapi1.HotelInfoes)
            {
                if (item.hotelLocation.Equals(location))
                {
                    Hotel hotel = new Hotel();
                    hotel.ID = item.hotelID;
                    hotel.Name = item.hotelName;
                    hotel.Location = item.hotelLocation;
                    hotel.HotelPrice = (float)item.hotelCost;
                    hotel.GardenRm = (int)item.hotelAvailGarden;
                    hotel.BeachRm = (int)item.hotelAvailBeach;
                    hotel.AllInclusive = item.hotelAllInclusive;
                    hotel.HotelImg = item.hotelImage;

                    hotelList1.Add(hotel);
                }
            }
            return hotelList1;
        }
    }
}
