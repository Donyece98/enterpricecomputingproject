﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace HotelAPI.Models
{
    /// <summary>
    /// Summary description for HotelRetrieveID
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class HotelRetrieveID : System.Web.Services.WebService
    {
        public List<Hotel> hotelList1 = new List<Hotel>();

        [WebMethod]
        public List<Hotel> GetHotel(string location)
        {
            HotelAPIEntities hotelapi = new HotelAPIEntities();

            foreach (var item in hotelapi.HotelInfoes)
            {

                if (item.hotelLocation.Equals(location))
                {
                    Hotel hotel = new Hotel();
                    hotel.ID = item.hotelID;
                    hotel.Name = item.hotelName;
                    hotel.Location = item.hotelLocation;
                    hotel.HotelPrice = (float)item.hotelCost;
                    hotel.GardenRm = (int)item.hotelAvailGarden;
                    hotel.BeachRm = (int)item.hotelAvailBeach;
                    hotel.AllInclusive = item.hotelAllInclusive;
                    hotel.HotelImg = item.hotelImage;

                    hotelList1.Add(hotel);

                }
            }
            return hotelList1;
        }
    }
}
