﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelAPI.Models
{
    public class Rooms
    {
        int roomID;
        int hotelID;
        string roomType;

        public int RoomID
        {
            get { return roomID; }
            set { roomID = value; }
        }

        public int HotelID
        {
            get { return hotelID; }
            set { hotelID = value; }
        }

        public string RoomType
        {
            get { return roomType; }
            set { roomType = value; }
        }
    }
}