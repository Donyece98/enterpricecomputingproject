﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace HotelAPI.Models
{
    /// <summary>
    /// Summary description for BookingReply
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class BookingReply : System.Web.Services.WebService
    {
        private SqlConnection con;
        private SqlCommand com;
        int bookingId;
        int hotelId;
        string roomId;
        string userId;
        float bookingCost;
        DateTime checkin;
        DateTime checkout;
        bool available;
        string bookStatus;

        public int BookingID
        {
            [WebMethod]
            get { return bookingId; }
            [WebMethod]
            set { bookingId = value; }
        }

        public int HotelID
        {
            [WebMethod]
            get { return hotelId; }
            [WebMethod]
            set { hotelId = value; }
        }

        public string RoomID
        {
            [WebMethod]
            get { return roomId; }
            [WebMethod]
            set { roomId = value; }
        }
        public string UserID
        {
            [WebMethod]
            get { return userId; }
            [WebMethod]
            set { userId = value; }
        }
        public float BookingCost
        {
            [WebMethod]
            get { return bookingCost; }
            [WebMethod]
            set { bookingCost = value; }
        }
        public DateTime CheckIn
        {
            [WebMethod]
            get { return checkin; }
            [WebMethod]
            set { checkin = value; }
        }
        public DateTime CheckOut
        {
            [WebMethod]
            get { return checkout; }
            [WebMethod]
            set { checkout = value; }
        }
        public bool Available
        {
            [WebMethod]
            get { return available; }
            [WebMethod]
            set { available = value; }
        }
        public string BookingStatus
        {
            [WebMethod]
            get { return bookStatus; }
            [WebMethod]
            set { bookStatus = value; }
        }
        private void connection()
        {
            //string constr=ConfigurationManager.ConnectionStrings
            con = new SqlConnection("Data Source=JM-DB044432\\SQLEXPRESS;Initial Catalog=HotelAPI;Integrated Security=True;MultipleActiveResultSets=True;Application Name=EntityFramework");
        }

        [WebMethod]
        public string AddBooking(HotelBooking booking)
        {
            connection();
            SqlCommand com1 = new SqlCommand("SELECT bookingId FROM BookingInfo", con);
            con.Open();
            SqlDataAdapter sda = new SqlDataAdapter(com1);
            DataTable dt = new DataTable();
            sda.Fill(dt);

            int bookingIDval = ((int)dt.Rows[dt.Rows.Count - 1]["bookingId"]) + 1;
            con.Close();

            com = new SqlCommand("INSERT INTO BookingInfo (bookingId, hotelId, roomId, UserEmail, bookingCost, [checkin ], [checkout ], available) values (@BookingID, @HotelID, @RoomID, @UserEmail, @BookingCost, @CheckIn, @CheckOut, @Available)", con);
            com.Parameters.AddWithValue("@BookingID", bookingIDval);
            com.Parameters.AddWithValue("@HotelID", booking.hotelId);
            com.Parameters.AddWithValue("@RoomID", booking.roomId);
            com.Parameters.AddWithValue("@UserEmail", booking.userId);
            com.Parameters.AddWithValue("@BookingCost", booking.bookingCost);
            com.Parameters.AddWithValue("@CheckIn", booking.checkin);
            com.Parameters.AddWithValue("@CheckOut", booking.checkout);
            com.Parameters.AddWithValue("@Available", booking.available);

            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {
                return "New Booking Added Successfully";
            }
            else
            {
                return "Booking Not Added";
            }
        }
    }
}
