﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelAPI.Models
{
    public class Hotel
    {
        int hotelID;
        string hotelName;
        string hotelLocation;
        int hotelGardenRm;
        int hotelBeachRm;
        float hotelCost;
        string allInclusive;
        string hotelImage;

        public int ID
        {
            get { return hotelID; }
            set { hotelID = value; }
        }

        public string Name
        {
            get { return hotelName; }
            set { hotelName = value; }
        }

        public string Location
        {
            get { return hotelLocation; }
            set { hotelLocation = value; }
        }

        public int GardenRm
        {
            get { return hotelGardenRm; }
            set { hotelGardenRm = value; }
        }

        public int BeachRm
        {
            get { return hotelBeachRm; }
            set { hotelBeachRm = value; }
        }

        public float HotelPrice
        {
            get { return hotelCost; }
            set { hotelCost = value; }
        }

        public string AllInclusive
        {
            get { return allInclusive; }
            set { allInclusive = value; }
        }

        public string HotelImg
        {
            get { return hotelImage; }
            set { hotelImage = value; }
        }
    }
}