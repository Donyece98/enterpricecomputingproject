﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace HotelAPI.Models
{
    /// <summary>
    /// Summary description for RoomRetrieve
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class RoomRetrieve : System.Web.Services.WebService
    {
        public List<Rooms> roomList = new List<Rooms>();
        [WebMethod]
        public List<Rooms> GetRoom(string hotelName, string roomT, DateTime checkin, DateTime checkout)
        {
            HotelAPIEntities hotelapi1 = new HotelAPIEntities();
            Hotel hotel = new Hotel();

            foreach (var item1 in hotelapi1.HotelInfoes)
            {
                if (item1.hotelName.Equals(hotelName))
                {
                    hotel.ID = item1.hotelID;
                    hotel.Name = item1.hotelName;
                    hotel.Location = item1.hotelLocation;
                    hotel.HotelPrice = (float)item1.hotelCost;
                    hotel.GardenRm = (int)item1.hotelAvailGarden;
                    hotel.BeachRm = (int)item1.hotelAvailBeach;
                    hotel.AllInclusive = item1.hotelAllInclusive;
                    hotel.HotelImg = item1.hotelImage;
                }
            }
            foreach (var item2 in hotelapi1.RoomInfoes)
            {
                if (item2.hotelID.Equals(hotel.ID) && (item2.roomType.TrimEnd(' ').Equals(roomT)))
                {
                    Rooms room = new Rooms();
                    room.RoomID = item2.roomId;
                    room.HotelID = item2.hotelID;
                    room.RoomType = item2.roomType;
                    roomList.Add(room);
                }

            }
            List<Rooms> roomListCopy = roomList;

            foreach (var item3 in hotelapi1.BookingInfoes)
            {
                for (int i = 0; i < roomList.Count; i++)
                {
                    if (item3.roomId.Equals(roomList[i].RoomID) && ((checkin >= item3.checkin_) && (checkout <= item3.checkout_)))
                    {
                        roomList.Remove(roomList[i]);
                    }
                }
            }
            return roomList;
        }
    }
}
