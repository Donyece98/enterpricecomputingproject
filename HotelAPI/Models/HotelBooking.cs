﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelAPI.Models
{
    public class HotelBooking
    {
        public int hotelId { get; set; }
        public string roomId { get; set; }
        public string userId { get; set; }
        public float bookingCost { get; set; }
        public DateTime checkin { get; set; }
        public DateTime checkout { get; set; }
        public bool available { get; set; }

    }
}